module github.com/beyondstorage/go-service-ftp

go 1.15

require (
	github.com/beyondstorage/go-credential v0.1.0
	github.com/beyondstorage/go-endpoint v1.1.0
	github.com/beyondstorage/go-integration-test/v4 v4.5.0
	github.com/beyondstorage/go-storage/v4 v4.7.0
	github.com/gopherjs/gopherjs v0.0.0-20210923143318-357ed63a84fb // indirect
	github.com/jlaffaye/ftp v0.0.0-20210307004419-5d4190119067
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/qingstor/go-mime v0.1.0
	github.com/smartystreets/assertions v1.2.0 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20210925032602-92d5a993a665 // indirect
	golang.org/x/tools v0.1.6 // indirect
)
